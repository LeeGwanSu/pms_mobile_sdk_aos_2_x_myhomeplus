package com.pmsdemo.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.pms.sdk.PMS;
import com.pms.sdk.common.util.CLog;
import com.pmsdemo.activity.SplashActivity;

public class PushDaeguReceiver extends BroadcastReceiver
{
    private static final String TAG = PushDaeguReceiver.class.getSimpleName();
    public static final String EXTRA_CLICK_NOTIFICATION_BAR = "CLICK_NOTIFICATION_BAR";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if ("com.pms.sdk.push".equals(intent.getAction()))
        {
            int unreadMsgCnt = PMS.getInstance(context).selectNewMsgCnt();
            updateBadgeCount(context, unreadMsgCnt);
        }
    }

    private void updateBadgeCount(Context context, int count)
    {
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", SplashActivity.class.getName());
        intent.putExtra("badge_count", count);
        CLog.d(intent.toString());
        context.sendBroadcast(intent);
    }
}
