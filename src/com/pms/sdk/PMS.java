package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.*;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

import java.io.Serializable;
import java.util.Stack;

public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;
	private static PMS instancePms;
	private static PMSPopup instancePmsPopup;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);

        initOption(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @param projectId
     * @return
     */
    public static PMS getInstance(Context context, String projectId) {
        CLog.d("getInstance projectId : " + projectId + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);
        PMSUtil.setGCMProjectId(context, projectId);
        return getInstance(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @return
     */
    public static PMS getInstance(final Context context) {
        CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
        if (instancePms == null) {
            instancePms = new PMS(context);
//            if (PhoneState.isAvailablePush()) {
//                PushReceiver.gcmRegister(context, PMSUtil.getGCMProjectId(context));
//            }
        }
        instancePms.setmContext(context);
        String token = PMSUtil.getGCMToken(context);
        if (TextUtils.isEmpty(token) || NO_TOKEN.equals(token))
        {
            // get token
            new FCMRequestToken(context, PMSUtil.getGCMProjectId(context), new FCMRequestToken.Callback() {
                @Override
                public void callback(boolean isSuccess, String message)
                {
                    CLog.i("FCMRequestToken "+isSuccess+" / "+message);
                }
            }).execute();
        }
        return instancePms;
    }

    public static PMSPopup getPopUpInstance() {
        return instancePmsPopup;
    }

    /**
     * clear
     *
     * @return
     */
    public static boolean clear() {
        try {
            PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
            instancePms.unregisterReceiver();
            instancePms = null;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * initialize option
     */
    private void initOption(Context context) {
        if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
            mPrefs.putString(PREF_NOTI_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
            mPrefs.putString(PREF_MSG_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
            mPrefs.putString(PREF_RING_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            mPrefs.putString(PREF_VIBE_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
            mPrefs.putString(PREF_ALERT_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
            mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
            mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
        }
        if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
            PMSUtil.setServerUrl(context, API_SERVER_URL);
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
            mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
        }
        if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
            mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
        }
    }

    private void setmContext(Context context) {
        this.mContext = context;
    }

    public void setPushToken(String pushToken) {
        PMSUtil.setGCMToken(mContext, pushToken);
    }

    private void unregisterReceiver() {
        Badge.getInstance(mContext).unregisterReceiver();
    }

    public void setPopupSetting(Boolean state, String title) {
        instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
    }

    /**
     * get cust id
     *
     * @return
     */
    public String getCustId() {
        CLog.i("getCustId");
        return PMSUtil.getCustId(mContext);
    }

    /**
     * cust id 세팅
     *
     * @param custId
     */
    public void setCustId(String custId) {
        CLog.i("setCustId");
        CLog.d("setCustId:custId=" + custId);
        PMSUtil.setCustId(mContext, custId);
    }

    public void setIsPopupActivity(Boolean ispopup) {
        PMSUtil.setPopupActivity(mContext, ispopup);
    }

    public boolean getIsPopupActivity() {
        return PMSUtil.getPopupActivity(mContext);
    }

    /**
     * set debugTag
     *
     * @param tagName
     */
    public void setDebugTAG(String tagName) {
        CLog.setTagName(tagName);
    }

    /**
     * set debug mode
     *
     * @param debugMode
     */
    public void setDebugMode(boolean debugMode) {
        CLog.setDebugMode(debugMode);
    }

    public OnReceivePushListener getOnReceivePushListener() {
        return onReceivePushListener;
    }

    public void setOnReceivePushListener(OnReceivePushListener onReceivePushListener) {
        this.onReceivePushListener = onReceivePushListener;
    }

    /**
     * get msg flag
     *
     * @return
     */
    public String getMsgFlag() {
        return mPrefs.getString(PREF_MSG_FLAG);
    }

    /**
     * get noti flag
     *
     * @return
     */
    public String getNotiFlag() {
        return mPrefs.getString(PREF_NOTI_FLAG);
    }

    public String getMktFlag() {
        return mPrefs.getString(IPMSConsts.PREF_MKT_FLAG);
    }

    public String getNtcFlag() {
        return mPrefs.getString(IPMSConsts.PREF_NTC_FLAG);
    }

    /**
     * set noti icon
     *
     * @param resId
     */
    public void setNotiIcon(int resId) {
        mPrefs.putInt(PREF_NOTI_ICON, resId);
    }

    /**
     * set large noti icon
     *
     * @param resId
     */
    public void setLargeNotiIcon(int resId) {
        mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
    }

    /**
     * set noti cound
     *
     * @param resId
     */
    public void setNotiSound(int resId) {
        mPrefs.putInt(PREF_NOTI_SOUND, resId);
    }

    /**
     * set noti receiver
     *
     * @param intentAction
     */
    public void setNotiReceiver(String intentAction) {
        mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
    }

    /**
     * set noti receiver class (Class name)
     * @param intentAction
     */
    public void setNotiReceiverClass (String intentAction) {
        mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentAction);
    }
    /**
     * set noti receiver (Intent action)
     * @param intentAction
     */
    public void setPushReceiverClass (String intentAction) {
        mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, intentAction);
    }

    /**
     * set ring mode
     *
     * @param isRingMode
     */
    public void setRingMode(boolean isRingMode) {
        mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
    }

    /**
     * get ring mode
     */
    public String getRingMode() {
        String strRet = "";
        strRet = mPrefs.getString(PREF_RING_FLAG);
        return strRet;
    }

    /**
     * set vibe mode
     *
     * @param isVibeMode
     */
    public void setVibeMode(boolean isVibeMode) {
        mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
    }

    /**
     * get vibe mode
     */
    public String getVibeMode() {
        String strRet = "";
        strRet = mPrefs.getString(PREF_VIBE_FLAG);
        return strRet;
    }

    /**
     * set popup noti
     *
     * @param isShowPopup
     */
    public void setPopupNoti(boolean isShowPopup) {
        mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
    }

    /**
     * get PopupNoti mode
     */
    public String getPopupNoti() {
        String strRet = "";
        strRet = mPrefs.getString(PREF_ALERT_FLAG);
        return strRet;
    }

    /**
     * set screen wakeup
     *
     * @param isScreenWakeup
     */
    public void setScreenWakeup(boolean isScreenWakeup) {
        mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
    }

    /**
     * set push popup activity
     *
     * @param classPath
     */
    public void setPushPopupActivity(String classPath) {
        mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
    }

    public void setUseBigText(Boolean state) {
        mPrefs.putString(PREF_USE_BIGTEXT, state ? "Y" : "N");
    }


    /**
     * set pushPopup showing time
     *
     * @param pushPopupShowingTime
     */
    public void setPushPopupShowingTime(int pushPopupShowingTime) {
        mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
    }

    /**
     * set inbox activity
     *
     * @param inboxActivity
     */
    public void setInboxActivity(String inboxActivity) {
        mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
    }

    public void bindBadge(Context c, int id) {
        Badge.getInstance(c).addBadge(c, id);
    }

    public void bindBadge(TextView badge) {
        Badge.getInstance(badge.getContext()).addBadge(badge);
    }

    /**
     * show Message Box
     *
     * @param c
     */
    public void showMsgBox(Context c) {
        showMsgBox(c, null);
    }

    public void showMsgBox(Context c, Bundle extras) {
        CLog.i("showMsgBox");
        if (PhoneState.isAvailablePush()) {
            try {
                Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

                Intent i = new Intent(mContext, inboxActivity);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (extras != null) {
                    i.putExtras(extras);
                }
                c.startActivity(i);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // Intent i = new Intent(INBOX_ACTIVITY);
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // if (extras != null) {
            // i.putExtras(extras);
            // }
            // c.startActivity(i);
        }
    }

    public void closeMsgBox(Context c) {
        Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
        c.sendBroadcast(i);
    }

    /**
     * start mqtt service
     */
    public void startMQTTService(Context context) {
        if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
            context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
        } else {
            context.stopService(new Intent(context, MQTTService.class));
        }
    }

    public void stopMQTTService(Context context) {
        context.stopService(new Intent(context, MQTTService.class));
    }

    /**
     * set ServerUrl
     *
     * @param serverUrl
     */
    public void setServerUrl(String serverUrl) {
        CLog.i("setServerUrl");
        CLog.d("setServerUrl:serverUrl=" + serverUrl);
        PMSUtil.setServerUrl(mContext, serverUrl);
    }

	/*
     * ===================================================== [start] database =====================================================
	 */

    /**
     * select MsgGrp list
     *
     * @return
     */
    public Cursor selectMsgGrpList() {
        return mDB.selectMsgGrpList();
    }

    /**
     * select MsgGrp
     *
     * @param msgCode
     * @return
     */
    public MsgGrp selectMsGrp(String msgCode) {
        return mDB.selectMsgGrp(msgCode);
    }

    /**
     * select new msg Cnt
     *
     * @return
     */
    public int selectNewMsgCnt() {
        return mDB.selectNewMsgCnt();
    }

    /**
     * select Msg List
     *
     * @param page
     * @param row
     * @return
     */
    public Cursor selectMsgList(int page, int row) {
        return mDB.selectMsgList(page, row);
    }

    /**
     * select Msg List
     *
     * @param msgCode
     * @return
     */
    public Cursor selectMsgList(String msgCode) {
        return mDB.selectMsgList(msgCode);
    }

    /**
     * select Msg
     *
     * @param msgId
     * @return
     */
    public Msg selectMsgWhereMsgId(String msgId) {
        return mDB.selectMsgWhereMsgId(msgId);
    }

    /**
     * select Msg
     *
     * @param userMsgID
     * @return
     */
    public Msg selectMsgWhereUserMsgId(String userMsgID) {
        return mDB.selectMsgWhereUserMsgId(userMsgID);
    }

    /**
     * select query
     *
     * @param sql
     * @return
     */
    public Cursor selectQuery(String sql) {
        return mDB.selectQuery(sql);
    }

    /**
     * update MsgGrp
     *
     * @param msgCode
     * @param values
     * @return
     */
    public long updateMsgGrp(String msgCode, ContentValues values) {
        return mDB.updateMsgGrp(msgCode, values);
    }

    /**
     * update read msg
     *
     * @param msgGrpCd
     * @param firstUserMsgId
     * @param lastUserMsgId
     * @return
     */
    public long updateReadMsg(String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
        return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
    }

    /**
     * update read msg where userMsgId
     *
     * @param userMsgId
     * @return
     */
    public long updateReadMsgWhereUserMsgId(String userMsgId) {
        return mDB.updateReadMsgWhereUserMsgId(userMsgId);
    }

    /**
     * update read msg where msgId
     *
     * @param msgId
     * @return
     */
    public long updateReadMsgWhereMsgId(String msgId) {
        return mDB.updateReadMsgWhereMsgId(msgId);
    }

    /**
     * delete Msg
     *
     * @param userMsgId
     * @return
     */
    public long deleteUserMsgId(String userMsgId) {
        return mDB.deleteUserMsgId(userMsgId);
    }

    /**
     * delete Msg
     *
     * @param MsgId
     * @return
     */
    public long deleteMsgId(String MsgId) {
        return mDB.deleteMsgId(MsgId);
    }

    /**
     * delete MsgGrp
     *
     * @param msgCode
     * @return
     */
    public long deleteMsgGrp(String msgCode) {
        return mDB.deleteMsgGrp(msgCode);
    }

    /**
     * delete expire Msg
     *
     * @return
     */
    public long deleteExpireMsg() {
        return mDB.deleteExpireMsg();
    }

    /**
     * delete empty MsgGrp
     *
     * @return
     */
    public void deleteEmptyMsgGrp() {
        mDB.deleteEmptyMsgGrp();
    }

    /**
     * delete all
     */
    public void deleteAll() {
        mDB.deleteAll();
    }

	/*
	 * ===================================================== [end] database =====================================================
	 */

    public String getPushToken () {
        return PMSUtil.getGCMToken(mContext);
    }

    private static Stack<Intent> serviceStack = new Stack<>();

    public static Stack<Intent> getServiceStack()
    {
        return serviceStack;
    }

    public static void setServiceStack(Intent serviceId)
    {
        serviceStack.push(serviceId);
    }

    public void setNotificationClickActivityClass(String action, Class activity) {
        PMSUtil.setActivityToMoveWhenClick(mContext, action, activity, false);
    }

    public void setNotificationClickActivityUseBackStack(String action, Class activity) {
        PMSUtil.setActivityToMoveWhenClick(mContext, action, activity, true);
    }

    public void setNotificationClickActivityFlag(int flags) {
        PMSUtil.setNotificationClickActivityFlag(mContext, flags);
    }

    public void setNotificationClickActivityFlagEnable(boolean value) {
        PMSUtil.setNotificationClickActivityFlagEnable(mContext, value);
    }

    public void createNotificationChannel(){
        PMSUtil.createNotificationChannel(mContext);
    }
}
