package com.pms.sdk.push;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMSPopup;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.ProPertiesFileUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.common.util.PMSPopupUtil;
import com.pms.sdk.common.util.PMSPopupUtil.btnEventListener;
import com.pms.sdk.common.util.PMSUtil;

/**
 * push popup activity
 *
 * @author Yang
 * @since 2013.12.11
 */
public class PushPopupActivity extends Activity implements IPMSConsts {

	private static final String APP_SCHEME = "tms:";

	private static Activity PushPopupActivity = null;

	private LayoutInflater inflate = null;

	private Context mContext = null;

	private String[] mStrBtnName = null;

	private RelativeLayout mRmainLayout = null;

	private LinearLayout.LayoutParams mlilp = null;

	private WebView mWv = null;

	private TextView mContextView = null;

	private PushMsg pushMsg = null;

	private Timer mPopupTimer = null;

	private int[] mnResId = null;

	private int mnBottomTextBtnCount = 0;
	private int mnBottomRichBtnCount = 0;
	private int mnMaxWidth = 0;
	private int pushPopupShowingTime = 0;
	private int mShowingTime = 0;

	private final static int FILL_PARENT = -1;
	private final static int WRAP_CONTENT = -2;

	private PMSPopupUtil PMSPopupUtil = null;

	@SuppressLint("ResourceAsColor")
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mContext = this;

		PMSPopupUtil = PMSPopup.getInstance(mContext).mPmsPopUtil;
//		PMSPopupUtil = new PMSPopupUtil();
//		PMSPopupUtil.setParmSetting(mContext);
//		PMSPopupUtil.mResData = PMSPopupUtil.getSaveMapData();
		PushPopupActivity = PushPopupActivity.this;
		PMSPopupUtil.setActivity(PushPopupActivity);

		int[] deviceSize = PhoneState.getDeviceSize(this);
		mnMaxWidth = (int) Math.round((deviceSize[0] < deviceSize[1] ? deviceSize[0] : deviceSize[1]) * 0.8);

		mRmainLayout = new RelativeLayout(this);
		mRmainLayout.setLayoutParams(new RelativeLayout.LayoutParams(WRAP_CONTENT, FILL_PARENT));
		mRmainLayout.setGravity(Gravity.CENTER);
		mRmainLayout.setPadding(50, 50, 50, 50);

		mPopupTimer = new Timer();

		Prefs pref = new Prefs(this);
		pushPopupShowingTime = pref.getInt(PREF_PUSH_POPUP_SHOWING_TIME);

		mContext.registerReceiver(changeContentReceiver, new IntentFilter(RECEIVER_CHANGE_POPUP));

		settingPopupUI(getIntent());

		setContentView(mRmainLayout);
	}

	@Override
	protected void onResume () {
		try {
			WebView.class.getMethod("onResume").invoke(mWv);
			mWv.resumeTimers();
		} catch (Exception e) {
		}
		super.onResume();
		this.overridePendingTransition(0, 0);
	}

	@Override
	public void onDestroy () {
		super.onDestroy();
		if (mWv != null) {
			mWv.loadData("", "Text/html", "UTF8");
		}
		mContext.unregisterReceiver(changeContentReceiver);
	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);
		settingPopupUI(intent);
	}

	private void dataSetting () {
		mnBottomTextBtnCount = PMSPopupUtil.getBottomTextBtnCount();
		mnBottomRichBtnCount = PMSPopupUtil.getBottomRichBtnCount();
		mnResId = PMSPopupUtil.getBottomBtnImageResource();
		mStrBtnName = PMSPopupUtil.getBottomBtnTextName();
	}

	public void settingPopupUI (Intent intent) {
		if (intent == null) {
			CLog.e("PushPopupActivity : getIntent is null!");
			return;
		}
		pushMsg = new PushMsg(intent.getExtras());

		CLog.i(pushMsg + "");

		PMSPopupUtil.setMsgId(pushMsg.msgId);
		Boolean BoolXmlFlag = PMSPopupUtil.getXmlAndDefaultFlag();
		BoolXmlFlag = BoolXmlFlag == null ? Boolean.FALSE : BoolXmlFlag;

		if (BoolXmlFlag) {
			View view = null;
			if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
				view = inflate.inflate(PMSPopupUtil.getLayoutXMLRichResId(), null);
				getXMLRichPopup(view, pushMsg.message);

			} else {
				view = inflate.inflate(PMSPopupUtil.getLayoutXMLTextResId(), null);
				getXMLTextPopup(view, pushMsg.notiMsg);
			}

			mRmainLayout.addView(view);
		} else {
			dataSetting();
			LinearLayout backGroundll = new LinearLayout(mContext);
			mlilp = new LinearLayout.LayoutParams(mnMaxWidth, WRAP_CONTENT);
			mlilp.gravity = Gravity.CENTER;
			backGroundll.setLayoutParams(mlilp);
			backGroundll.setOrientation(LinearLayout.VERTICAL);

			if (PMSPopupUtil.mResData.containsKey(POPUP_BACKGROUND_COLOR)) {
				backGroundll.setBackgroundColor(Color.parseColor(PMSPopupUtil.getPopUpBackColor()));
			} else if (PMSPopupUtil.mResData.containsKey(POPUP_BACKGROUND_RES_ID)) {
				backGroundll.setBackgroundResource(PMSPopupUtil.getPopupBackImgResource());
			}

			if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
				backGroundll.addView(getRichPopup(pushMsg.message));
			} else {
				backGroundll.addView(getTextPopup(pushMsg.notiMsg));
			}

			mRmainLayout.addView(backGroundll);
		}

		mShowingTime = pushPopupShowingTime;
		mPopupTimer.schedule(new TimerTask() {
			@Override
			public void run () {
				if (mShowingTime > -1) {
					mShowingTime -= 1000;
				} else {
					mPopupTimer.cancel();
					finish();
				}
			}
		}, 0, 1000);
	}

	private View getTopLayoutSetting () {
		LinearLayout topMainll = new LinearLayout(mContext);
		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.gravity = Gravity.CENTER;

		topMainll.setOrientation(LinearLayout.VERTICAL);
		topMainll.setLayoutParams(mlilp);
		topMainll.setPadding(15, 15, 15, 15);

		if (PMSPopupUtil.mResData.containsKey(TOP_BACKGROUND_COLOR)) {
			topMainll.setBackgroundColor(Color.parseColor(PMSPopupUtil.getTopBackColor()));
		} else if (PMSPopupUtil.mResData.containsKey(TOP_BACKGROUND_RES_ID)) {
			topMainll.setBackgroundResource(PMSPopupUtil.getTopBackImgResource());
		}

		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.gravity = Gravity.CENTER;
		if (TOP_TITLE_TEXT.equals(PMSPopupUtil.getTopTitleType())) {
			TextView txt = new TextView(mContext);
			txt.setLayoutParams(mlilp);
			txt.setGravity(Gravity.CENTER);
			txt.setTextColor(Color.parseColor(PMSPopupUtil.getTopTitleTextColor()));
			txt.setTextSize(PMSPopupUtil.getTopTitleTextSize());
			txt.setText(PMSPopupUtil.getTopTitleName());

			topMainll.addView(txt);
		} else if (TOP_TITLE_IMAGE.equals(PMSPopupUtil.getTopTitleType())) {
			ImageView iv = new ImageView(mContext);
			iv.setLayoutParams(mlilp);
			iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
			iv.setImageResource(PMSPopupUtil.getTopTitleImgResource());

			topMainll.addView(iv);
		}

		return topMainll;
	}

	private View getTextPopup (String msg) {
		CLog.i("getTextPopup");
		LinearLayout contentMainll = new LinearLayout(mContext);
		mlilp = new LinearLayout.LayoutParams(0, WRAP_CONTENT);
		mlilp.weight = 1;

		contentMainll.setOrientation(LinearLayout.VERTICAL);
		contentMainll.setLayoutParams(new LayoutParams(FILL_PARENT, FILL_PARENT));

		if (PMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_COLOR)) {
			contentMainll.setBackgroundColor(Color.parseColor(PMSPopupUtil.getContentBackColor()));
		} else if (PMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_RES_ID)) {
			contentMainll.setBackgroundResource(PMSPopupUtil.getContentBackImgResource());
		}

		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.setMargins(15, 15, 15, 15);
		mContextView = new TextView(mContext);
		mContextView.setLayoutParams(mlilp);
		int colorText = Color.BLACK;
		if (PMSPopupUtil.getContentTextColor() != null) {
			colorText = Color.parseColor(PMSPopupUtil.getContentTextColor());
		}
		mContextView.setTextColor(colorText);
		mContextView.setTextSize(PMSPopupUtil.getContentTextSize());
		mContextView.setText(msg);

		Boolean bTopLayout = PMSPopupUtil.getTopLayoutFlag();
		bTopLayout = bTopLayout == null ? Boolean.FALSE : bTopLayout;
		if (bTopLayout) {
			contentMainll.addView(getTopLayoutSetting());
		}
		contentMainll.addView(mContextView);
		contentMainll.addView(bottomLayoutSetting());

		return contentMainll;
	}

	private void getXMLTextPopup (View view, String msg) {
		CLog.i("getXMLTextPopup");

		mContextView = ((TextView) PMSPopupUtil.getSerachView(view, TEXTVIEW, "ContentTextView"));
		mContextView.setText(msg);

		String[] viewtype = PMSPopupUtil.getXMLTextButtonType();
		String[] viewTagName = PMSPopupUtil.getXMLTextButtonTagName();
		for (int i = 0; i < viewtype.length; i++) {
			final int count = i;
			View buttonView = PMSPopupUtil.getSerachView(view, PMSPopupUtil.getViewType(viewtype[i]), viewTagName[i]);
			buttonView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = PMSPopupUtil.getTextBottomBtnClickListener(count);
					btnEvent.onClick();
				}
			});
		}
	}

	private View getRichPopup (String url) {
		CLog.i("getRichPopup");
		mlilp = new LinearLayout.LayoutParams(0, WRAP_CONTENT);
		mlilp.weight = 1;

		LinearLayout contentMainll = new LinearLayout(mContext);
		contentMainll.setOrientation(LinearLayout.VERTICAL);
		contentMainll.setLayoutParams(new LayoutParams(FILL_PARENT, FILL_PARENT));

		if (PMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_COLOR)) {
			contentMainll.setBackgroundColor(Color.parseColor(PMSPopupUtil.getContentBackColor()));
		} else if (PMSPopupUtil.mResData.containsKey(CONTENT_BACKGROUND_RES_ID)) {
			contentMainll.setBackgroundResource(PMSPopupUtil.getContentBackImgResource());
		}

		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.weight = 1;
		mWv = new WebView(mContext);
		mWv.setLayoutParams(mlilp);

		final ProgressBar prg = new ProgressBar(mContext, null, android.R.attr.progressBarStyleHorizontal);
		prg.setProgress(50);
		prg.setLayoutParams(mlilp);

		webViewSetting(prg, url);

		if (PMSPopupUtil.getTopLayoutFlag()) {
			contentMainll.addView(getTopLayoutSetting());
		}
		contentMainll.addView(mWv);
		contentMainll.addView(prg);
		contentMainll.addView(bottomLayoutSetting());

		return contentMainll;
	}

	private void getXMLRichPopup (View view, String url) {
		CLog.i("getXMLRichPopup");

		ProgressBar prg = (ProgressBar) PMSPopupUtil.getSerachView(view, PROGRESSBAR, "ContentProgressBar");
		mWv = (WebView) PMSPopupUtil.getSerachView(view, WEBVIEW, "ContentWebView");
		webViewSetting(prg, url);

		String[] viewtype = PMSPopupUtil.getXMLRichButtonType();
		String[] viewTagName = PMSPopupUtil.getXMLRichButtonTagName();
		for (int i = 0; i < viewtype.length; i++) {
			final int count = i;
			View buttonView = PMSPopupUtil.getSerachView(view, PMSPopupUtil.getViewType(viewtype[i]), viewTagName[i]);
			buttonView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = PMSPopupUtil.getTextBottomBtnClickListener(count);
					btnEvent.onClick();
				}
			});
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	private void webViewSetting (final ProgressBar prg, final String url) {
		mWv.clearCache(true);
		mWv.clearHistory();
		mWv.clearFormData();

		mWv.setBackgroundColor(android.R.style.Theme_Translucent);
		mWv.setHorizontalScrollBarEnabled(false);
		mWv.setVerticalScrollBarEnabled(false);

		WebSettings settings = mWv.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setSupportMultipleWindows(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setGeolocationEnabled(true);
		settings.setDomStorageEnabled(true);
		settings.setUseWideViewPort(true);
		settings.setPluginState(PluginState.ON);

		mWv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading (WebView view, String url) {
				CLog.e("webViewClient:url=" + url);
				if (url.startsWith(APP_SCHEME)) {
					return true;
				}
				return false;
			}
		});

		mWv.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged (WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				prg.setProgress(newProgress);
				if (newProgress >= 100) {
					prg.setVisibility(View.GONE);
				}
			}
		});

		PMSWebViewBridge tmsBridge = new PMSWebViewBridge(mContext);
		mWv.addJavascriptInterface(tmsBridge, "tms");

		runOnUiThread(new Runnable() {
			@Override
			public void run () {
				if (Msg.TYPE_L.equals(pushMsg.msgType) && url.startsWith("http")) {
					mWv.loadUrl(url);
				} else {
					mWv.loadDataWithBaseURL(null, url, "text/html", "utf-8", null);
				}
			}
		});

		mWv.setOnTouchListener(new OnTouchListener() {
			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch (View v, MotionEvent event) {
				if (mShowingTime < pushPopupShowingTime) {
					mShowingTime = pushPopupShowingTime;
				}
				if (event.getAction() != MotionEvent.ACTION_MOVE) {
					((WebView) v).setWebViewClient(new WebViewClient() {
						@Override
						public boolean shouldOverrideUrlLoading (WebView view, String url) {
							CLog.e("webViewClient:url=" + url);
							PMSUtil.arrayToPrefs(mContext, PREF_READ_LIST, PMSUtil.getReadParam(pushMsg.msgId));
							Intent intent = new Intent(Intent.ACTION_VIEW);
							Uri uri = Uri.parse(url);
							intent.setData(uri);
							startActivity(intent);
							return true;
						}
					});
				}
				return false;
			}
		});
	}

	private View bottomLayoutSetting () {
		LinearLayout bottomMainll = new LinearLayout(mContext);
		mlilp = new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT);
		mlilp.gravity = Gravity.CENTER;
		bottomMainll.setLayoutParams(mlilp);
		bottomMainll.setOrientation(LinearLayout.HORIZONTAL);
		bottomMainll.setGravity(Gravity.CENTER);

		if (PMSPopupUtil.mResData.containsKey(BOTTOM_BACKGROUND_COLOR)) {
			bottomMainll.setBackgroundColor(Color.parseColor(PMSPopupUtil.getBottomBackColor()));
		} else if (PMSPopupUtil.mResData.containsKey(BOTTOM_BACKGROUND_RES_ID)) {
			bottomMainll.setBackgroundResource(PMSPopupUtil.getBottomBackImgResource());
		}

		int count = 0;
		if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
			count = mnBottomRichBtnCount;
		} else {
			count = mnBottomTextBtnCount;
		}

		LinearLayout[] bottomSubll = new LinearLayout[count];
		if (count != 0) {
			String strBtnName = "";
			for (int i = 0; i < count; i++) {
				strBtnName = "";
				try {
					strBtnName = mStrBtnName[i];
				} catch (NullPointerException e) {
					CLog.e(e.getMessage());
				} catch (IndexOutOfBoundsException e) {
					CLog.e(e.getMessage());
				}
				bottomSubll[i] = (LinearLayout) bottomLinearLayoutSetting(strBtnName, i, count);
				bottomMainll.addView(bottomSubll[i]);
			}
		}

		return bottomMainll;
	}

	private View bottomLinearLayoutSetting (String btnName, final int count, int totalcount) {
		LinearLayout ll = new LinearLayout(mContext);
		if (totalcount == 2) {
			mlilp = new LinearLayout.LayoutParams(0, WRAP_CONTENT);
			mlilp.weight = 50;
		} else {
			mlilp = new LinearLayout.LayoutParams(mnMaxWidth / 2, WRAP_CONTENT);
		}

		mlilp.setMargins(15, 15, 15, 15);
		ll.setLayoutParams(mlilp);

		View view = null;
		if (PMSPopupUtil.mResData.containsKey(BOTTOM_BTN_RES_ID)) {
			ll.setBackgroundResource(mnResId[count]);
			if (PMSPopupUtil.getBottomTextViewFlag()) {
				ll.addView(bottomImageBackToText(btnName));
			}
			view = ll;
		} else {
			Button btn = new Button(mContext);
			btn.setText(btnName);
			ll.addView(btn, new LinearLayout.LayoutParams(FILL_PARENT, WRAP_CONTENT));
			view = btn;
		}

		if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = PMSPopupUtil.getRichBottomBtnClickListener(count);
					btnEvent.onClick();
				}
			});
		} else {
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					btnEventListener btnEvent = PMSPopupUtil.getTextBottomBtnClickListener(count);
					btnEvent.onClick();
				}
			});
		}
		return ll;
	}

	private View bottomImageBackToText (String imgText) {
		TextView text = new TextView(mContext);
		text.setLayoutParams(new LayoutParams(FILL_PARENT, WRAP_CONTENT));
		text.setText(imgText);
		text.setTextColor(Color.parseColor(PMSPopupUtil.getBottomBtnTextColor()));
		text.setGravity(Gravity.CENTER);
		text.setPadding(10, 10, 10, 10);
		text.setTextSize(PMSPopupUtil.getBottomBtnTextSize());

		return text;
	}

	private String changeUrl (String url) {
		String temp = "";
		int ver = Build.VERSION.SDK_INT;
		if (ver > Build.VERSION_CODES.JELLY_BEAN) {
			int len = url.indexOf("target-densitydpi");
			if (len != -1) {
				temp = url.replace(", target-densitydpi=device-dpi", "");
			} else {
				temp = url;
			}
		} else {
			temp = url;
		}
		return temp;
	}

	BroadcastReceiver changeContentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive (Context context, Intent intent) {
			pushMsg = new PushMsg(intent.getExtras());

			new Handler().post(new Runnable() {
				@Override
				public void run () {
					CLog.e("Push Popup Receiver!!!");
					if (Msg.TYPE_T.equals(pushMsg.msgType)) {
						mContextView.setText(pushMsg.notiMsg);
					} else if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
						mWv.loadData("", "Text/html", "UTF8");

						if (Msg.TYPE_L.equals(pushMsg.msgType) && pushMsg.message.startsWith("http")) {
							mWv.loadUrl(pushMsg.message);
						} else {
							mWv.loadDataWithBaseURL(null, pushMsg.message, "text/html", "utf-8", null);
						}
					}
				}
			});
		}
	};
}