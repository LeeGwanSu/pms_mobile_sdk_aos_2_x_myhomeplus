package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;

import org.json.JSONObject;

public class SetConfig extends BaseRequest {

	public SetConfig(Context context) {
		super(context);
	}

	/**
	 * get param
	 *
	 * @param msgFlag
	 * @param notiFlag
	 * @return
	 */
	public JSONObject getParam (String msgFlag, String notiFlag) {
		return getParam(msgFlag, notiFlag, "", "");
	}

	public JSONObject getParam (String msgFlag, String notiFlag, String mktFlag, String ntcFlag) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgFlag", msgFlag);
			jobj.put("notiFlag", notiFlag);
			jobj.put("mktFlag", mktFlag);
			jobj.put("ntcFlag", ntcFlag);
			jobj.put("ntcVer", FLAG_Y);
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 *
	 * @param apiCallback
	 */
	public void request (String msgFlag, String notiFlag, final APICallback apiCallback) {
		request(msgFlag, notiFlag, mPrefs.getString(IPMSConsts.PREF_MKT_FLAG), mPrefs.getString(IPMSConsts.PREF_NTC_FLAG), apiCallback);
	}

	public void request (String msgFlag, String notiFlag, String mktFlag, String ntcFlag, final APICallback apiCallback) {
		try {
			apiManager.call(API_SET_CONFIG, getParam(msgFlag, notiFlag, mktFlag, ntcFlag), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 *
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			if(json.has("msgFlag")){
				mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			}
			if(json.has("notiFlag")){
				mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			}
			if(json.has("mktFlag")){
				mPrefs.putString(IPMSConsts.PREF_MKT_FLAG, json.getString("mktFlag"));
			}
			if(json.has("ntcFlag")){
				mPrefs.putString(IPMSConsts.PREF_NTC_FLAG, json.getString("ntcFlag"));
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
